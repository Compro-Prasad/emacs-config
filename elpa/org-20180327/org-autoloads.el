;;; org-autoloads.el --- automatically extracted autoloads
;;
;;; Code:

(add-to-list 'load-path (directory-file-name
                         (or (file-name-directory #$) (car load-path))))


;;;### (autoloads nil "ob-C" "ob-C.el" (0 0 0 0))
;;; Generated autoloads from ob-C.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ob-C" '("org-babel-")))

;;;***

;;;### (autoloads nil "ob-J" "ob-J.el" (0 0 0 0))
;;; Generated autoloads from ob-J.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ob-J" '("obj-" "org-babel-")))

;;;***

;;;### (autoloads nil "ob-R" "ob-R.el" (0 0 0 0))
;;; Generated autoloads from ob-R.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ob-R" '("ob-R-" "org-babel-")))

;;;***

;;;### (autoloads nil "ob-abc" "ob-abc.el" (0 0 0 0))
;;; Generated autoloads from ob-abc.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ob-abc" '("org-babel-")))

;;;***

;;;### (autoloads nil "ob-asymptote" "ob-asymptote.el" (0 0 0 0))
;;; Generated autoloads from ob-asymptote.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ob-asymptote" '("org-babel-")))

;;;***

;;;### (autoloads nil "ob-awk" "ob-awk.el" (0 0 0 0))
;;; Generated autoloads from ob-awk.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ob-awk" '("org-babel-")))

;;;***

;;;### (autoloads nil "ob-calc" "ob-calc.el" (0 0 0 0))
;;; Generated autoloads from ob-calc.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ob-calc" '("org-babel-")))

;;;***

;;;### (autoloads nil "ob-clojure" "ob-clojure.el" (0 0 0 0))
;;; Generated autoloads from ob-clojure.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ob-clojure" '("org-babel-")))

;;;***

;;;### (autoloads nil "ob-comint" "ob-comint.el" (0 0 0 0))
;;; Generated autoloads from ob-comint.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ob-comint" '("org-babel-comint-")))

;;;***

;;;### (autoloads nil "ob-coq" "ob-coq.el" (0 0 0 0))
;;; Generated autoloads from ob-coq.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ob-coq" '("org-babel-" "coq-program-name")))

;;;***

;;;### (autoloads "actual autoloads are elsewhere" "ob-core" "ob-core.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from ob-core.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ob-core" '("org-")))

;;;***

;;;### (autoloads nil "ob-css" "ob-css.el" (0 0 0 0))
;;; Generated autoloads from ob-css.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ob-css" '("org-babel-")))

;;;***

;;;### (autoloads nil "ob-ditaa" "ob-ditaa.el" (0 0 0 0))
;;; Generated autoloads from ob-ditaa.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ob-ditaa" '("org-")))

;;;***

;;;### (autoloads nil "ob-dot" "ob-dot.el" (0 0 0 0))
;;; Generated autoloads from ob-dot.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ob-dot" '("org-babel-")))

;;;***

;;;### (autoloads nil "ob-ebnf" "ob-ebnf.el" (0 0 0 0))
;;; Generated autoloads from ob-ebnf.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ob-ebnf" '("org-babel-")))

;;;***

;;;### (autoloads nil "ob-emacs-lisp" "ob-emacs-lisp.el" (0 0 0 0))
;;; Generated autoloads from ob-emacs-lisp.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ob-emacs-lisp" '("org-babel-")))

;;;***

;;;### (autoloads nil "ob-eval" "ob-eval.el" (0 0 0 0))
;;; Generated autoloads from ob-eval.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ob-eval" '("org-babel-")))

;;;***

;;;### (autoloads nil "ob-exp" "ob-exp.el" (0 0 0 0))
;;; Generated autoloads from ob-exp.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ob-exp" '("org-")))

;;;***

;;;### (autoloads nil "ob-forth" "ob-forth.el" (0 0 0 0))
;;; Generated autoloads from ob-forth.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ob-forth" '("org-babel-")))

;;;***

;;;### (autoloads nil "ob-fortran" "ob-fortran.el" (0 0 0 0))
;;; Generated autoloads from ob-fortran.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ob-fortran" '("org-babel-")))

;;;***

;;;### (autoloads nil "ob-gnuplot" "ob-gnuplot.el" (0 0 0 0))
;;; Generated autoloads from ob-gnuplot.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ob-gnuplot" '("org-babel-" "*org-babel-gnuplot-")))

;;;***

;;;### (autoloads nil "ob-groovy" "ob-groovy.el" (0 0 0 0))
;;; Generated autoloads from ob-groovy.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ob-groovy" '("org-babel-")))

;;;***

;;;### (autoloads nil "ob-haskell" "ob-haskell.el" (0 0 0 0))
;;; Generated autoloads from ob-haskell.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ob-haskell" '("org-babel-")))

;;;***

;;;### (autoloads nil "ob-hledger" "ob-hledger.el" (0 0 0 0))
;;; Generated autoloads from ob-hledger.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ob-hledger" '("org-babel-")))

;;;***

;;;### (autoloads nil "ob-io" "ob-io.el" (0 0 0 0))
;;; Generated autoloads from ob-io.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ob-io" '("org-babel-")))

;;;***

;;;### (autoloads nil "ob-java" "ob-java.el" (0 0 0 0))
;;; Generated autoloads from ob-java.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ob-java" '("org-babel-")))

;;;***

;;;### (autoloads nil "ob-js" "ob-js.el" (0 0 0 0))
;;; Generated autoloads from ob-js.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ob-js" '("org-babel-")))

;;;***

;;;### (autoloads "actual autoloads are elsewhere" "ob-keys" "ob-keys.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from ob-keys.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ob-keys" '("org-babel-")))

;;;***

;;;### (autoloads nil "ob-latex" "ob-latex.el" (0 0 0 0))
;;; Generated autoloads from ob-latex.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ob-latex" '("org-babel-")))

;;;***

;;;### (autoloads nil "ob-ledger" "ob-ledger.el" (0 0 0 0))
;;; Generated autoloads from ob-ledger.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ob-ledger" '("org-babel-")))

;;;***

;;;### (autoloads nil "ob-lilypond" "ob-lilypond.el" (0 0 0 0))
;;; Generated autoloads from ob-lilypond.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ob-lilypond" '("org-babel-" "lilypond-mode")))

;;;***

;;;### (autoloads nil "ob-lisp" "ob-lisp.el" (0 0 0 0))
;;; Generated autoloads from ob-lisp.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ob-lisp" '("org-babel-")))

;;;***

;;;### (autoloads "actual autoloads are elsewhere" "ob-lob" "ob-lob.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from ob-lob.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ob-lob" '("org-babel-")))

;;;***

;;;### (autoloads nil "ob-lua" "ob-lua.el" (0 0 0 0))
;;; Generated autoloads from ob-lua.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ob-lua" '("org-babel-")))

;;;***

;;;### (autoloads nil "ob-makefile" "ob-makefile.el" (0 0 0 0))
;;; Generated autoloads from ob-makefile.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ob-makefile" '("org-babel-")))

;;;***

;;;### (autoloads nil "ob-maxima" "ob-maxima.el" (0 0 0 0))
;;; Generated autoloads from ob-maxima.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ob-maxima" '("org-babel-")))

;;;***

;;;### (autoloads nil "ob-mscgen" "ob-mscgen.el" (0 0 0 0))
;;; Generated autoloads from ob-mscgen.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ob-mscgen" '("org-babel-")))

;;;***

;;;### (autoloads nil "ob-ocaml" "ob-ocaml.el" (0 0 0 0))
;;; Generated autoloads from ob-ocaml.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ob-ocaml" '("org-babel-")))

;;;***

;;;### (autoloads nil "ob-octave" "ob-octave.el" (0 0 0 0))
;;; Generated autoloads from ob-octave.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ob-octave" '("org-babel-")))

;;;***

;;;### (autoloads nil "ob-org" "ob-org.el" (0 0 0 0))
;;; Generated autoloads from ob-org.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ob-org" '("org-babel-")))

;;;***

;;;### (autoloads nil "ob-perl" "ob-perl.el" (0 0 0 0))
;;; Generated autoloads from ob-perl.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ob-perl" '("org-babel-")))

;;;***

;;;### (autoloads nil "ob-picolisp" "ob-picolisp.el" (0 0 0 0))
;;; Generated autoloads from ob-picolisp.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ob-picolisp" '("org-babel-")))

;;;***

;;;### (autoloads nil "ob-plantuml" "ob-plantuml.el" (0 0 0 0))
;;; Generated autoloads from ob-plantuml.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ob-plantuml" '("org-")))

;;;***

;;;### (autoloads nil "ob-processing" "ob-processing.el" (0 0 0 0))
;;; Generated autoloads from ob-processing.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ob-processing" '("org-babel-")))

;;;***

;;;### (autoloads nil "ob-python" "ob-python.el" (0 0 0 0))
;;; Generated autoloads from ob-python.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ob-python" '("org-babel-")))

;;;***

;;;### (autoloads nil "ob-ref" "ob-ref.el" (0 0 0 0))
;;; Generated autoloads from ob-ref.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ob-ref" '("org-babel-")))

;;;***

;;;### (autoloads nil "ob-ruby" "ob-ruby.el" (0 0 0 0))
;;; Generated autoloads from ob-ruby.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ob-ruby" '("org-babel-")))

;;;***

;;;### (autoloads nil "ob-sass" "ob-sass.el" (0 0 0 0))
;;; Generated autoloads from ob-sass.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ob-sass" '("org-babel-")))

;;;***

;;;### (autoloads nil "ob-scheme" "ob-scheme.el" (0 0 0 0))
;;; Generated autoloads from ob-scheme.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ob-scheme" '("org-babel-")))

;;;***

;;;### (autoloads nil "ob-screen" "ob-screen.el" (0 0 0 0))
;;; Generated autoloads from ob-screen.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ob-screen" '("org-babel-")))

;;;***

;;;### (autoloads nil "ob-sed" "ob-sed.el" (0 0 0 0))
;;; Generated autoloads from ob-sed.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ob-sed" '("org-babel-")))

;;;***

;;;### (autoloads nil "ob-shell" "ob-shell.el" (0 0 0 0))
;;; Generated autoloads from ob-shell.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ob-shell" '("org-babel-")))

;;;***

;;;### (autoloads nil "ob-shen" "ob-shen.el" (0 0 0 0))
;;; Generated autoloads from ob-shen.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ob-shen" '("org-babel-")))

;;;***

;;;### (autoloads nil "ob-sql" "ob-sql.el" (0 0 0 0))
;;; Generated autoloads from ob-sql.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ob-sql" '("org-babel-")))

;;;***

;;;### (autoloads nil "ob-sqlite" "ob-sqlite.el" (0 0 0 0))
;;; Generated autoloads from ob-sqlite.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ob-sqlite" '("org-babel-")))

;;;***

;;;### (autoloads nil "ob-stan" "ob-stan.el" (0 0 0 0))
;;; Generated autoloads from ob-stan.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ob-stan" '("org-babel-")))

;;;***

;;;### (autoloads nil "ob-table" "ob-table.el" (0 0 0 0))
;;; Generated autoloads from ob-table.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ob-table" '("org-")))

;;;***

;;;### (autoloads "actual autoloads are elsewhere" "ob-tangle" "ob-tangle.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from ob-tangle.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ob-tangle" '("org-babel-")))

;;;***

;;;### (autoloads nil "ob-vala" "ob-vala.el" (0 0 0 0))
;;; Generated autoloads from ob-vala.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ob-vala" '("org-babel-")))

;;;***

;;;### (autoloads nil "org" "org.el" (0 0 0 0))
;;; Generated autoloads from org.el

(autoload 'org-babel-do-load-languages "org" "\
Load the languages defined in `org-babel-load-languages'.\n\n(fn SYM VALUE)" nil nil)

(autoload 'org-babel-load-file "org" "\
Load Emacs Lisp source code blocks in the Org FILE.\nThis function exports the source code using `org-babel-tangle'\nand then loads the resulting file using `load-file'.  With prefix\narg (noninteractively: 2nd arg) COMPILE the tangled Emacs Lisp\nfile to byte-code before it is loaded.\n\n(fn FILE &optional COMPILE)" t nil)

(autoload 'org-version "org" "\
Show the Org version.\nInteractively, or when MESSAGE is non-nil, show it in echo area.\nWith prefix argument, or when HERE is non-nil, insert it at point.\nIn non-interactive uses, a reduced version string is output unless\nFULL is given.\n\n(fn &optional HERE FULL MESSAGE)" t nil)

(autoload 'turn-on-orgtbl "org" "\
Unconditionally turn on `orgtbl-mode'.\n\n(fn)" nil nil)

(autoload 'org-clock-persistence-insinuate "org" "\
Set up hooks for clock persistence.\n\n(fn)" nil nil)

(autoload 'org-mode "org" "\
Outline-based notes management and organizer, alias\n\"Carsten's outline-mode for keeping track of everything.\"\n\nOrg mode develops organizational tasks around a NOTES file which\ncontains information about projects as plain text.  Org mode is\nimplemented on top of Outline mode, which is ideal to keep the content\nof large files well structured.  It supports ToDo items, deadlines and\ntime stamps, which magically appear in the diary listing of the Emacs\ncalendar.  Tables are easily created with a built-in table editor.\nPlain text URL-like links connect to websites, emails (VM), Usenet\nmessages (Gnus), BBDB entries, and any files related to the project.\nFor printing and sharing of notes, an Org file (or a part of it)\ncan be exported as a structured ASCII or HTML file.\n\nThe following commands are available:\n\n\\{org-mode-map}\n\n(fn)" t nil)

(autoload 'org-cycle "org" "\
TAB-action and visibility cycling for Org mode.\n\nThis is the command invoked in Org mode by the `TAB' key.  Its main\npurpose is outline visibility cycling, but it also invokes other actions\nin special contexts.\n\nWhen this function is called with a `\\[universal-argument]' prefix, rotate the entire\nbuffer through 3 states (global cycling)\n  1. OVERVIEW: Show only top-level headlines.\n  2. CONTENTS: Show all headlines of all levels, but no body text.\n  3. SHOW ALL: Show everything.\n\nWith a `\\[universal-argument] \\[universal-argument]' prefix argument, switch to the startup visibility,\ndetermined by the variable `org-startup-folded', and by any VISIBILITY\nproperties in the buffer.\n\nWith a `\\[universal-argument] \\[universal-argument] \\[universal-argument]' prefix argument, show the entire buffer, including\nany drawers.\n\nWhen inside a table, re-align the table and move to the next field.\n\nWhen point is at the beginning of a headline, rotate the subtree started\nby this line through 3 different states (local cycling)\n  1. FOLDED:   Only the main headline is shown.\n  2. CHILDREN: The main headline and the direct children are shown.\n               From this state, you can move to one of the children\n               and zoom in further.\n  3. SUBTREE:  Show the entire subtree, including body text.\nIf there is no subtree, switch directly from CHILDREN to FOLDED.\n\nWhen point is at the beginning of an empty headline and the variable\n`org-cycle-level-after-item/entry-creation' is set, cycle the level\nof the headline by demoting and promoting it to likely levels.  This\nspeeds up creation document structure by pressing `TAB' once or several\ntimes right after creating a new headline.\n\nWhen there is a numeric prefix, go up to a heading with level ARG, do\na `show-subtree' and return to the previous cursor position.  If ARG\nis negative, go up that many levels.\n\nWhen point is not at the beginning of a headline, execute the global\nbinding for `TAB', which is re-indenting the line.  See the option\n`org-cycle-emulate-tab' for details.\n\nAs a special case, if point is at the beginning of the buffer and there is\nno headline in line 1, this function will act as if called with prefix arg\n(`\\[universal-argument] TAB', same as `S-TAB') also when called without prefix arg, but only\nif the variable `org-cycle-global-at-bob' is t.\n\n(fn &optional ARG)" t nil)

(autoload 'org-global-cycle "org" "\
Cycle the global visibility.  For details see `org-cycle'.\nWith `\\[universal-argument]' prefix ARG, switch to startup visibility.\nWith a numeric prefix, show all headlines up to that level.\n\n(fn &optional ARG)" t nil)
(put 'orgstruct-heading-prefix-regexp 'safe-local-variable 'stringp)

(autoload 'orgstruct-mode "org" "\
Toggle the minor mode `orgstruct-mode'.\nThis mode is for using Org mode structure commands in other\nmodes.  The following keys behave as if Org mode were active, if\nthe cursor is on a headline, or on a plain list item (both as\ndefined by Org mode).\n\n(fn &optional ARG)" t nil)

(autoload 'turn-on-orgstruct "org" "\
Unconditionally turn on `orgstruct-mode'.\n\n(fn)" nil nil)

(autoload 'turn-on-orgstruct++ "org" "\
Unconditionally turn on `orgstruct++-mode'.\n\n(fn)" nil nil)

(autoload 'org-run-like-in-org-mode "org" "\
Run a command, pretending that the current buffer is in Org mode.\nThis will temporarily bind local variables that are typically bound in\nOrg mode to the values they have in Org mode, and then interactively\ncall CMD.\n\n(fn CMD)" nil nil)

(autoload 'org-store-link "org" "\
Store an org-link to the current location.\n\\<org-mode-map>\nThis link is added to `org-stored-links' and can later be inserted\ninto an Org buffer with `org-insert-link' (`\\[org-insert-link]').\n\nFor some link types, a `\\[universal-argument]' prefix ARG is interpreted.  A single\n`\\[universal-argument]' negates `org-context-in-file-links' for file links or\n`org-gnus-prefer-web-links' for links to Usenet articles.\n\nA `\\[universal-argument] \\[universal-argument]' prefix ARG forces skipping storing functions that are not\npart of Org core.\n\nA `\\[universal-argument] \\[universal-argument] \\[universal-argument]' prefix ARG forces storing a link for each line in the\nactive region.\n\n(fn ARG)" t nil)

(autoload 'org-insert-link-global "org" "\
Insert a link like Org mode does.\nThis command can be called in any mode to insert a link in Org syntax.\n\n(fn)" t nil)

(autoload 'org-open-at-point-global "org" "\
Follow a link or time-stamp like Org mode does.\nThis command can be called in any mode to follow an external link\nor a time-stamp that has Org mode syntax.  Its behavior is\nundefined when called on internal links (e.g., fuzzy links).\nRaise an error when there is nothing to follow.  \n\n(fn)" t nil)

(autoload 'org-open-link-from-string "org" "\
Open a link in the string S, as if it was in Org mode.\n\n(fn S &optional ARG REFERENCE-BUFFER)" t nil)

(autoload 'org-switchb "org" "\
Switch between Org buffers.\n\nWith `\\[universal-argument]' prefix, restrict available buffers to files.\n\nWith `\\[universal-argument] \\[universal-argument]' prefix, restrict available buffers to agenda files.\n\n(fn &optional ARG)" t nil)

(autoload 'org-cycle-agenda-files "org" "\
Cycle through the files in `org-agenda-files'.\nIf the current buffer visits an agenda file, find the next one in the list.\nIf the current buffer does not, find the first agenda file.\n\n(fn)" t nil)

(autoload 'org-submit-bug-report "org" "\
Submit a bug report on Org via mail.\n\nDon't hesitate to report any problems or inaccurate documentation.\n\nIf you don't have setup sending mail from (X)Emacs, please copy the\noutput buffer into your mail program, as it gives us important\ninformation about your Org version and configuration.\n\n(fn)" t nil)

(autoload 'org-reload "org" "\
Reload all Org Lisp files.\nWith prefix arg UNCOMPILED, load the uncompiled versions.\n\n(fn &optional UNCOMPILED)" t nil)

(autoload 'org-customize "org" "\
Call the customize function with org as argument.\n\n(fn)" t nil)

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "org" '("org" "turn-on-org-cdlatex")))

;;;***

;;;### (autoloads nil "org-agenda" "org-agenda.el" (0 0 0 0))
;;; Generated autoloads from org-agenda.el

(autoload 'org-toggle-sticky-agenda "org-agenda" "\
Toggle `org-agenda-sticky'.\n\n(fn &optional ARG)" t nil)

(autoload 'org-agenda "org-agenda" "\
Dispatch agenda commands to collect entries to the agenda buffer.\nPrompts for a command to execute.  Any prefix arg will be passed\non to the selected command.  The default selections are:\n\na     Call `org-agenda-list' to display the agenda for current day or week.\nt     Call `org-todo-list' to display the global todo list.\nT     Call `org-todo-list' to display the global todo list, select only\n      entries with a specific TODO keyword (the user gets a prompt).\nm     Call `org-tags-view' to display headlines with tags matching\n      a condition  (the user is prompted for the condition).\nM     Like `m', but select only TODO entries, no ordinary headlines.\ne     Export views to associated files.\ns     Search entries for keywords.\nS     Search entries for keywords, only with TODO keywords.\n/     Multi occur across all agenda files and also files listed\n      in `org-agenda-text-search-extra-files'.\n<     Restrict agenda commands to buffer, subtree, or region.\n      Press several times to get the desired effect.\n>     Remove a previous restriction.\n#     List \"stuck\" projects.\n!     Configure what \"stuck\" means.\nC     Configure custom agenda commands.\n\nMore commands can be added by configuring the variable\n`org-agenda-custom-commands'.  In particular, specific tags and TODO keyword\nsearches can be pre-defined in this way.\n\nIf the current buffer is in Org mode and visiting a file, you can also\nfirst press `<' once to indicate that the agenda should be temporarily\n(until the next use of `\\[org-agenda]') restricted to the current file.\nPressing `<' twice means to restrict to the current subtree or region\n(if active).\n\n(fn &optional ARG ORG-KEYS RESTRICTION)" t nil)

(autoload 'org-batch-agenda "org-agenda" "\
Run an agenda command in batch mode and send the result to STDOUT.\nIf CMD-KEY is a string of length 1, it is used as a key in\n`org-agenda-custom-commands' and triggers this command.  If it is a\nlonger string it is used as a tags/todo match string.\nParameters are alternating variable names and values that will be bound\nbefore running the agenda command.\n\n(fn CMD-KEY &rest PARAMETERS)" nil t)

(autoload 'org-batch-agenda-csv "org-agenda" "\
Run an agenda command in batch mode and send the result to STDOUT.\nIf CMD-KEY is a string of length 1, it is used as a key in\n`org-agenda-custom-commands' and triggers this command.  If it is a\nlonger string it is used as a tags/todo match string.\nParameters are alternating variable names and values that will be bound\nbefore running the agenda command.\n\nThe output gives a line for each selected agenda item.  Each\nitem is a list of comma-separated values, like this:\n\ncategory,head,type,todo,tags,date,time,extra,priority-l,priority-n\n\ncategory     The category of the item\nhead         The headline, without TODO kwd, TAGS and PRIORITY\ntype         The type of the agenda entry, can be\n                todo               selected in TODO match\n                tagsmatch          selected in tags match\n                diary              imported from diary\n                deadline           a deadline on given date\n                scheduled          scheduled on given date\n                timestamp          entry has timestamp on given date\n                closed             entry was closed on given date\n                upcoming-deadline  warning about deadline\n                past-scheduled     forwarded scheduled item\n                block              entry has date block including g. date\ntodo         The todo keyword, if any\ntags         All tags including inherited ones, separated by colons\ndate         The relevant date, like 2007-2-14\ntime         The time, like 15:00-16:50\nextra        Sting with extra planning info\npriority-l   The priority letter if any was given\npriority-n   The computed numerical priority\nagenda-day   The day in the agenda where this is listed\n\n(fn CMD-KEY &rest PARAMETERS)" nil t)

(autoload 'org-store-agenda-views "org-agenda" "\
Store agenda views.\n\n(fn &rest PARAMETERS)" t nil)

(autoload 'org-batch-store-agenda-views "org-agenda" "\
Run all custom agenda commands that have a file argument.\n\n(fn &rest PARAMETERS)" nil t)

(autoload 'org-agenda-list "org-agenda" "\
Produce a daily/weekly view from all files in variable `org-agenda-files'.\nThe view will be for the current day or week, but from the overview buffer\nyou will be able to go to other days/weeks.\n\nWith a numeric prefix argument in an interactive call, the agenda will\nspan ARG days.  Lisp programs should instead specify SPAN to change\nthe number of days.  SPAN defaults to `org-agenda-span'.\n\nSTART-DAY defaults to TODAY, or to the most recent match for the weekday\ngiven in `org-agenda-start-on-weekday'.\n\nWhen WITH-HOUR is non-nil, only include scheduled and deadline\nitems if they have an hour specification like [h]h:mm.\n\n(fn &optional ARG START-DAY SPAN WITH-HOUR)" t nil)

(autoload 'org-search-view "org-agenda" "\
Show all entries that contain a phrase or words or regular expressions.\n\nWith optional prefix argument TODO-ONLY, only consider entries that are\nTODO entries.  The argument STRING can be used to pass a default search\nstring into this function.  If EDIT-AT is non-nil, it means that the\nuser should get a chance to edit this string, with cursor at position\nEDIT-AT.\n\nThe search string can be viewed either as a phrase that should be found as\nis, or it can be broken into a number of snippets, each of which must match\nin a Boolean way to select an entry.  The default depends on the variable\n`org-agenda-search-view-always-boolean'.\nEven if this is turned off (the default) you can always switch to\nBoolean search dynamically by preceding the first word with  \"+\" or \"-\".\n\nThe default is a direct search of the whole phrase, where each space in\nthe search string can expand to an arbitrary amount of whitespace,\nincluding newlines.\n\nIf using a Boolean search, the search string is split on whitespace and\neach snippet is searched separately, with logical AND to select an entry.\nWords prefixed with a minus must *not* occur in the entry.  Words without\na prefix or prefixed with a plus must occur in the entry.  Matching is\ncase-insensitive.  Words are enclosed by word delimiters (i.e. they must\nmatch whole words, not parts of a word) if\n`org-agenda-search-view-force-full-words' is set (default is nil).\n\nBoolean search snippets enclosed by curly braces are interpreted as\nregular expressions that must or (when preceded with \"-\") must not\nmatch in the entry.  Snippets enclosed into double quotes will be taken\nas a whole, to include whitespace.\n\n- If the search string starts with an asterisk, search only in headlines.\n- If (possibly after the leading star) the search string starts with an\n  exclamation mark, this also means to look at TODO entries only, an effect\n  that can also be achieved with a prefix argument.\n- If (possibly after star and exclamation mark) the search string starts\n  with a colon, this will mean that the (non-regexp) snippets of the\n  Boolean search must match as full words.\n\nThis command searches the agenda files, and in addition the files\nlisted in `org-agenda-text-search-extra-files' unless a restriction lock\nis active.\n\n(fn &optional TODO-ONLY STRING EDIT-AT)" t nil)

(autoload 'org-todo-list "org-agenda" "\
Show all (not done) TODO entries from all agenda file in a single list.\nThe prefix arg can be used to select a specific TODO keyword and limit\nthe list to these.  When using `\\[universal-argument]', you will be prompted\nfor a keyword.  A numeric prefix directly selects the Nth keyword in\n`org-todo-keywords-1'.\n\n(fn &optional ARG)" t nil)

(autoload 'org-tags-view "org-agenda" "\
Show all headlines for all `org-agenda-files' matching a TAGS criterion.\nThe prefix arg TODO-ONLY limits the search to TODO entries.\n\n(fn &optional TODO-ONLY MATCH)" t nil)

(autoload 'org-agenda-list-stuck-projects "org-agenda" "\
Create agenda view for projects that are stuck.\nStuck projects are project that have no next actions.  For the definitions\nof what a project is and how to check if it stuck, customize the variable\n`org-stuck-projects'.\n\n(fn &rest IGNORE)" t nil)

(autoload 'org-diary "org-agenda" "\
Return diary information from org files.\nThis function can be used in a \"sexp\" diary entry in the Emacs calendar.\nIt accesses org files and extracts information from those files to be\nlisted in the diary.  The function accepts arguments specifying what\nitems should be listed.  For a list of arguments allowed here, see the\nvariable `org-agenda-entry-types'.\n\nThe call in the diary file should look like this:\n\n   &%%(org-diary) ~/path/to/some/orgfile.org\n\nUse a separate line for each org file to check.  Or, if you omit the file name,\nall files listed in `org-agenda-files' will be checked automatically:\n\n   &%%(org-diary)\n\nIf you don't give any arguments (as in the example above), the default value\nof `org-agenda-entry-types' is used: (:deadline :scheduled :timestamp :sexp).\nSo the example above may also be written as\n\n   &%%(org-diary :deadline :timestamp :sexp :scheduled)\n\nThe function expects the lisp variables `entry' and `date' to be provided\nby the caller, because this is how the calendar works.  Don't use this\nfunction from a program - use `org-agenda-get-day-entries' instead.\n\n(fn &rest ARGS)" nil nil)

(autoload 'org-agenda-check-for-timestamp-as-reason-to-ignore-todo-item "org-agenda" "\
Do we have a reason to ignore this TODO entry because it has a time stamp?\n\n(fn &optional END)" nil nil)

(autoload 'org-agenda-set-restriction-lock "org-agenda" "\
Set restriction lock for agenda, to current subtree or file.\nRestriction will be the file if TYPE is `file', or if type is the\nuniversal prefix \\='(4), or if the cursor is before the first headline\nin the file.  Otherwise, restriction will be to the current subtree.\n\n(fn &optional TYPE)" t nil)

(autoload 'org-calendar-goto-agenda "org-agenda" "\
Compute the Org agenda for the calendar date displayed at the cursor.\nThis is a command that has to be installed in `calendar-mode-map'.\n\n(fn)" t nil)

(autoload 'org-agenda-to-appt "org-agenda" "\
Activate appointments found in `org-agenda-files'.\n\nWith a `\\[universal-argument]' prefix, refresh the list of appointments.\n\nIf FILTER is t, interactively prompt the user for a regular\nexpression, and filter out entries that don't match it.\n\nIf FILTER is a string, use this string as a regular expression\nfor filtering entries out.\n\nIf FILTER is a function, filter out entries against which\ncalling the function returns nil.  This function takes one\nargument: an entry from `org-agenda-get-day-entries'.\n\nFILTER can also be an alist with the car of each cell being\neither `headline' or `category'.  For example:\n\n  \\='((headline \"IMPORTANT\")\n    (category \"Work\"))\n\nwill only add headlines containing IMPORTANT or headlines\nbelonging to the \"Work\" category.\n\nARGS are symbols indicating what kind of entries to consider.\nBy default `org-agenda-to-appt' will use :deadline*, :scheduled*\n(i.e., deadlines and scheduled items with a hh:mm specification)\nand :timestamp entries.  See the docstring of `org-diary' for\ndetails and examples.\n\nIf an entry has a APPT_WARNTIME property, its value will be used\nto override `appt-message-warning-time'.\n\n(fn &optional REFRESH FILTER &rest ARGS)" t nil)

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "org-agenda" '("org-")))

;;;***

;;;### (autoloads "actual autoloads are elsewhere" "org-archive"
;;;;;;  "org-archive.el" (0 0 0 0))
;;; Generated autoloads from org-archive.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "org-archive" '("org-")))

;;;***

;;;### (autoloads "actual autoloads are elsewhere" "org-attach" "org-attach.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from org-attach.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "org-attach" '("org-attach-")))

;;;***

;;;### (autoloads "actual autoloads are elsewhere" "org-bbdb" "org-bbdb.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from org-bbdb.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "org-bbdb" '("org-bbdb-")))

;;;***

;;;### (autoloads nil "org-bibtex" "org-bibtex.el" (0 0 0 0))
;;; Generated autoloads from org-bibtex.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "org-bibtex" '("org-")))

;;;***

;;;### (autoloads nil "org-capture" "org-capture.el" (0 0 0 0))
;;; Generated autoloads from org-capture.el

(autoload 'org-capture-string "org-capture" "\
Capture STRING with the template selected by KEYS.\n\n(fn STRING &optional KEYS)" t nil)

(autoload 'org-capture "org-capture" "\
Capture something.\n\\<org-capture-mode-map>\nThis will let you select a template from `org-capture-templates', and\nthen file the newly captured information.  The text is immediately\ninserted at the target location, and an indirect buffer is shown where\nyou can edit it.  Pressing `\\[org-capture-finalize]' brings you back to the previous\nstate of Emacs, so that you can continue your work.\n\nWhen called interactively with a `\\[universal-argument]' prefix argument GOTO, don't\ncapture anything, just go to the file/headline where the selected\ntemplate stores its notes.\n\nWith a `\\[universal-argument] \\[universal-argument]' prefix argument, go to the last note stored.\n\nWhen called with a `C-0' (zero) prefix, insert a template at point.\n\nWhen called with a `C-1' (one) prefix, force prompting for a date when\na datetree entry is made.\n\nELisp programs can set KEYS to a string associated with a template\nin `org-capture-templates'.  In this case, interactive selection\nwill be bypassed.\n\nIf `org-capture-use-agenda-date' is non-nil, capturing from the\nagenda will use the date at point as the default date.  Then, a\n`C-1' prefix will tell the capture process to use the HH:MM time\nof the day at point (if any) or the current HH:MM time.\n\n(fn &optional GOTO KEYS)" t nil)

(autoload 'org-capture-import-remember-templates "org-capture" "\
Set `org-capture-templates' to be similar to `org-remember-templates'.\n\n(fn)" t nil)

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "org-capture" '("org-")))

;;;***

;;;### (autoloads "actual autoloads are elsewhere" "org-clock" "org-clock.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from org-clock.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "org-clock" '("org-")))

;;;***

;;;### (autoloads nil "org-colview" "org-colview.el" (0 0 0 0))
;;; Generated autoloads from org-colview.el

(autoload 'org-columns-remove-overlays "org-colview" "\
Remove all currently active column overlays.\n\n(fn)" t nil)

(autoload 'org-columns-get-format-and-top-level "org-colview" "\
\n\n(fn)" nil nil)

(autoload 'org-columns "org-colview" "\
Turn on column view on an Org mode file.\n\nColumn view applies to the whole buffer if point is before the\nfirst headline.  Otherwise, it applies to the first ancestor\nsetting \"COLUMNS\" property.  If there is none, it defaults to\nthe current headline.  With a `\\[universal-argument]' prefix argument, turn on column\nview for the whole buffer unconditionally.\n\nWhen COLUMNS-FMT-STRING is non-nil, use it as the column format.\n\n(fn &optional GLOBAL COLUMNS-FMT-STRING)" t nil)

(autoload 'org-columns-compute "org-colview" "\
Summarize the values of PROPERTY hierarchically.\nAlso update existing values for PROPERTY according to the first\ncolumn specification.\n\n(fn PROPERTY)" t nil)

(autoload 'org-dblock-write:columnview "org-colview" "\
Write the column view table.\nPARAMS is a property list of parameters:\n\n:id       the :ID: property of the entry where the columns view\n	  should be built.  When the symbol `local', call locally.\n	  When `global' call column view with the cursor at the beginning\n	  of the buffer (usually this means that the whole buffer switches\n	  to column view).  When \"file:path/to/file.org\", invoke column\n	  view at the start of that file.  Otherwise, the ID is located\n	  using `org-id-find'.\n:hlines   When t, insert a hline before each item.  When a number, insert\n	  a hline before each level <= that number.\n:indent   When non-nil, indent each ITEM field according to its level.\n:vlines   When t, make each column a colgroup to enforce vertical lines.\n:maxlevel When set to a number, don't capture headlines below this level.\n:skip-empty-rows\n	  When t, skip rows where all specifiers other than ITEM are empty.\n:width    apply widths specified in columns format using <N> specifiers.\n:format   When non-nil, specify the column view format to use.\n\n(fn PARAMS)" nil nil)

(autoload 'org-columns-insert-dblock "org-colview" "\
Create a dynamic block capturing a column view table.\n\n(fn)" t nil)

(autoload 'org-agenda-columns "org-colview" "\
Turn on or update column view in the agenda.\n\n(fn)" t nil)

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "org-colview" '("org-")))

;;;***

;;;### (autoloads nil "org-compat" "org-compat.el" (0 0 0 0))
;;; Generated autoloads from org-compat.el

(autoload 'org-check-version "org-compat" "\
Try very hard to provide sensible version strings.\n\n(fn)" nil t)

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "org-compat" '("org-")))

;;;***

;;;### (autoloads nil "org-crypt" "org-crypt.el" (0 0 0 0))
;;; Generated autoloads from org-crypt.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "org-crypt" '("org-")))

;;;***

;;;### (autoloads nil "org-ctags" "org-ctags.el" (0 0 0 0))
;;; Generated autoloads from org-ctags.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "org-ctags" '("org-ctags-")))

;;;***

;;;### (autoloads "actual autoloads are elsewhere" "org-datetree"
;;;;;;  "org-datetree.el" (0 0 0 0))
;;; Generated autoloads from org-datetree.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "org-datetree" '("org-datetree-")))

;;;***

;;;### (autoloads nil "org-docview" "org-docview.el" (0 0 0 0))
;;; Generated autoloads from org-docview.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "org-docview" '("org-docview-")))

;;;***

;;;### (autoloads nil "org-duration" "org-duration.el" (0 0 0 0))
;;; Generated autoloads from org-duration.el

(autoload 'org-duration-set-regexps "org-duration" "\
Set duration related regexps.\n\n(fn)" t nil)

(autoload 'org-duration-p "org-duration" "\
Non-nil when string S is a time duration.\n\n(fn S)" nil nil)

(autoload 'org-duration-to-minutes "org-duration" "\
Return number of minutes of DURATION string.\n\nWhen optional argument CANONICAL is non-nil, ignore\n`org-duration-units' and use standard time units value.\n\nA bare number is translated into minutes.  The empty string is\ntranslated into 0.0.\n\nReturn value as a float.  Raise an error if duration format is\nnot recognized.\n\n(fn DURATION &optional CANONICAL)" nil nil)

(autoload 'org-duration-from-minutes "org-duration" "\
Return duration string for a given number of MINUTES.\n\nFormat duration according to `org-duration-format' or FMT, when\nnon-nil.\n\nWhen optional argument CANONICAL is non-nil, ignore\n`org-duration-units' and use standard time units value.\n\nRaise an error if expected format is unknown.\n\n(fn MINUTES &optional FMT CANONICAL)" nil nil)

(autoload 'org-duration-h:mm-only-p "org-duration" "\
Non-nil when every duration in TIMES has \"H:MM\" or \"H:MM:SS\" format.\n\nTIMES is a list of duration strings.\n\nReturn nil if any duration is expressed with units, as defined in\n`org-duration-units'.  Otherwise, if any duration is expressed\nwith \"H:MM:SS\" format, return `h:mm:ss'.  Otherwise, return\n`h:mm'.\n\n(fn TIMES)" nil nil)

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "org-duration" '("org-duration-")))

;;;***

;;;### (autoloads "actual autoloads are elsewhere" "org-element"
;;;;;;  "org-element.el" (0 0 0 0))
;;; Generated autoloads from org-element.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "org-element" '("org-element-")))

;;;***

;;;### (autoloads nil "org-entities" "org-entities.el" (0 0 0 0))
;;; Generated autoloads from org-entities.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "org-entities" '("org-entit")))

;;;***

;;;### (autoloads nil "org-eshell" "org-eshell.el" (0 0 0 0))
;;; Generated autoloads from org-eshell.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "org-eshell" '("org-eshell-")))

;;;***

;;;### (autoloads nil "org-eww" "org-eww.el" (0 0 0 0))
;;; Generated autoloads from org-eww.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "org-eww" '("org-eww-")))

;;;***

;;;### (autoloads nil "org-faces" "org-faces.el" (0 0 0 0))
;;; Generated autoloads from org-faces.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "org-faces" '("org-")))

;;;***

;;;### (autoloads "actual autoloads are elsewhere" "org-feed" "org-feed.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from org-feed.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "org-feed" '("org-feed-")))

;;;***

;;;### (autoloads "actual autoloads are elsewhere" "org-footnote"
;;;;;;  "org-footnote.el" (0 0 0 0))
;;; Generated autoloads from org-footnote.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "org-footnote" '("org-footnote-")))

;;;***

;;;### (autoloads nil "org-gnus" "org-gnus.el" (0 0 0 0))
;;; Generated autoloads from org-gnus.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "org-gnus" '("org-gnus-")))

;;;***

;;;### (autoloads nil "org-habit" "org-habit.el" (0 0 0 0))
;;; Generated autoloads from org-habit.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "org-habit" '("org-")))

;;;***

;;;### (autoloads "actual autoloads are elsewhere" "org-id" "org-id.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from org-id.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "org-id" '("org-id-")))

;;;***

;;;### (autoloads "actual autoloads are elsewhere" "org-indent" "org-indent.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from org-indent.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "org-indent" '("org-")))

;;;***

;;;### (autoloads nil "org-info" "org-info.el" (0 0 0 0))
;;; Generated autoloads from org-info.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "org-info" '("org-info-")))

;;;***

;;;### (autoloads nil "org-inlinetask" "org-inlinetask.el" (0 0 0
;;;;;;  0))
;;; Generated autoloads from org-inlinetask.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "org-inlinetask" '("org-inlinetask-")))

;;;***

;;;### (autoloads "actual autoloads are elsewhere" "org-irc" "org-irc.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from org-irc.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "org-irc" '("org-irc-")))

;;;***

;;;### (autoloads nil "org-lint" "org-lint.el" (0 0 0 0))
;;; Generated autoloads from org-lint.el

(autoload 'org-lint "org-lint" "\
Check current Org buffer for syntax mistakes.\n\nBy default, run all checkers.  With a `\\[universal-argument]' prefix ARG, select one\ncategory of checkers only.  With a `\\[universal-argument] \\[universal-argument]' prefix, run one precise\nchecker by its name.\n\nARG can also be a list of checker names, as symbols, to run.\n\n(fn &optional ARG)" t nil)

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "org-lint" '("org-lint-")))

;;;***

;;;### (autoloads nil "org-list" "org-list.el" (0 0 0 0))
;;; Generated autoloads from org-list.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "org-list" '("org-")))

;;;***

;;;### (autoloads nil "org-macro" "org-macro.el" (0 0 0 0))
;;; Generated autoloads from org-macro.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "org-macro" '("org-macro-")))

;;;***

;;;### (autoloads nil "org-macs" "org-macs.el" (0 0 0 0))
;;; Generated autoloads from org-macs.el

(autoload 'org-load-noerror-mustsuffix "org-macs" "\
Load FILE with optional arguments NOERROR and MUSTSUFFIX.\n\n(fn FILE)" nil t)

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "org-macs" '("org-")))

;;;***

;;;### (autoloads nil "org-mhe" "org-mhe.el" (0 0 0 0))
;;; Generated autoloads from org-mhe.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "org-mhe" '("org-mhe-")))

;;;***

;;;### (autoloads "actual autoloads are elsewhere" "org-mobile" "org-mobile.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from org-mobile.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "org-mobile" '("org-mobile-")))

;;;***

;;;### (autoloads nil "org-mouse" "org-mouse.el" (0 0 0 0))
;;; Generated autoloads from org-mouse.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "org-mouse" '("org-mouse-")))

;;;***

;;;### (autoloads nil "org-pcomplete" "org-pcomplete.el" (0 0 0 0))
;;; Generated autoloads from org-pcomplete.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "org-pcomplete" '("org-" "pcomplete/org-mode/")))

;;;***

;;;### (autoloads "actual autoloads are elsewhere" "org-plot" "org-plot.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from org-plot.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "org-plot" '("org-plot")))

;;;***

;;;### (autoloads nil "org-protocol" "org-protocol.el" (0 0 0 0))
;;; Generated autoloads from org-protocol.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "org-protocol" '("org-protocol-")))

;;;***

;;;### (autoloads nil "org-rmail" "org-rmail.el" (0 0 0 0))
;;; Generated autoloads from org-rmail.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "org-rmail" '("org-rmail-")))

;;;***

;;;### (autoloads nil "org-src" "org-src.el" (0 0 0 0))
;;; Generated autoloads from org-src.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "org-src" '("org-")))

;;;***

;;;### (autoloads "actual autoloads are elsewhere" "org-table" "org-table.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from org-table.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "org-table" '("org")))

;;;***

;;;### (autoloads "actual autoloads are elsewhere" "org-timer" "org-timer.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from org-timer.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "org-timer" '("org-timer-")))

;;;***

;;;### (autoloads nil "org-version" "org-version.el" (0 0 0 0))
;;; Generated autoloads from org-version.el

(autoload 'org-release "org-version" "\
The release version of Org.\nInserted by installing Org mode or when a release is made.\n\n(fn)" nil nil)

(autoload 'org-git-version "org-version" "\
The Git version of Org mode.\nInserted by installing Org or when a release is made.\n\n(fn)" nil nil)

(defvar org-odt-data-dir "/usr/share/emacs/etc/org" "\
The location of ODT styles.")

;;;***

;;;### (autoloads nil "org-w3m" "org-w3m.el" (0 0 0 0))
;;; Generated autoloads from org-w3m.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "org-w3m" '("org-w3m-")))

;;;***

;;;### (autoloads "actual autoloads are elsewhere" "ox" "ox.el" (0
;;;;;;  0 0 0))
;;; Generated autoloads from ox.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ox" '("org-export-")))

;;;***

;;;### (autoloads "actual autoloads are elsewhere" "ox-ascii" "ox-ascii.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from ox-ascii.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ox-ascii" '("org-ascii-")))

;;;***

;;;### (autoloads "actual autoloads are elsewhere" "ox-beamer" "ox-beamer.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from ox-beamer.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ox-beamer" '("org-beamer-")))

;;;***

;;;### (autoloads "actual autoloads are elsewhere" "ox-html" "ox-html.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from ox-html.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ox-html" '("org-html-")))

;;;***

;;;### (autoloads "actual autoloads are elsewhere" "ox-icalendar"
;;;;;;  "ox-icalendar.el" (0 0 0 0))
;;; Generated autoloads from ox-icalendar.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ox-icalendar" '("org-icalendar-")))

;;;***

;;;### (autoloads "actual autoloads are elsewhere" "ox-latex" "ox-latex.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from ox-latex.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ox-latex" '("org-latex-")))

;;;***

;;;### (autoloads "actual autoloads are elsewhere" "ox-man" "ox-man.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from ox-man.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ox-man" '("org-man-")))

;;;***

;;;### (autoloads "actual autoloads are elsewhere" "ox-md" "ox-md.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from ox-md.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ox-md" '("org-md-")))

;;;***

;;;### (autoloads "actual autoloads are elsewhere" "ox-odt" "ox-odt.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from ox-odt.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ox-odt" '("org-odt-")))

;;;***

;;;### (autoloads "actual autoloads are elsewhere" "ox-org" "ox-org.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from ox-org.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ox-org" '("org-org-")))

;;;***

;;;### (autoloads "actual autoloads are elsewhere" "ox-publish" "ox-publish.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from ox-publish.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ox-publish" '("org-publish-")))

;;;***

;;;### (autoloads "actual autoloads are elsewhere" "ox-texinfo" "ox-texinfo.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from ox-texinfo.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ox-texinfo" '("org-texinfo-")))

;;;***

;;;### (autoloads nil nil ("ob-core.el" "ob-keys.el" "ob-lob.el"
;;;;;;  "ob-matlab.el" "ob-tangle.el" "ob.el" "org-archive.el" "org-attach.el"
;;;;;;  "org-bbdb.el" "org-clock.el" "org-datetree.el" "org-element.el"
;;;;;;  "org-feed.el" "org-footnote.el" "org-id.el" "org-indent.el"
;;;;;;  "org-install.el" "org-irc.el" "org-loaddefs.el" "org-mobile.el"
;;;;;;  "org-pkg.el" "org-plot.el" "org-table.el" "org-timer.el"
;;;;;;  "ox-ascii.el" "ox-beamer.el" "ox-html.el" "ox-icalendar.el"
;;;;;;  "ox-latex.el" "ox-man.el" "ox-md.el" "ox-odt.el" "ox-org.el"
;;;;;;  "ox-publish.el" "ox-texinfo.el" "ox.el") (0 0 0 0))

;;;***

;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; org-autoloads.el ends here
