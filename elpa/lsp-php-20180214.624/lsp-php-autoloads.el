;;; lsp-php-autoloads.el --- automatically extracted autoloads
;;
;;; Code:
(add-to-list 'load-path (directory-file-name (or (file-name-directory #$) (car load-path))))

;;;### (autoloads nil "lsp-php" "lsp-php.el" (23176 60734 285299
;;;;;;  880000))
;;; Generated autoloads from lsp-php.el

(defvar lsp-php-server-install-dir (file-truename (locate-user-emacs-file "php-language-server/")) "\
Install directory for php-language-server.
The slash is expected at the end.")

(custom-autoload 'lsp-php-server-install-dir "lsp-php" t)

;;;***

;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; End:
;;; lsp-php-autoloads.el ends here
