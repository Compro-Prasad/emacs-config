;;; -*- no-byte-compile: t -*-
(define-package "lsp-php" "20180214.624" "php support for lsp-mode" '((emacs "25.1") (lsp-mode "3.0")) :commit "ff5f5ba417342aa05869a1a9c6a0fc2d42c8acc3" :url "https://github.com/tszg/lsp-php" :keywords '("convenience" "php"))
