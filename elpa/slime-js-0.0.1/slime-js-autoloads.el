;;; slime-js-autoloads.el --- automatically extracted autoloads
;;
;;; Code:

(add-to-list 'load-path (directory-file-name
                         (or (file-name-directory #$) (car load-path))))


;;;### (autoloads nil "slime-js" "slime-js.el" (0 0 0 0))
;;; Generated autoloads from slime-js.el

(autoload 'slime-js-init "slime-js" "\
\n\n(fn)" nil nil)

(add-hook 'slime-load-hook 'slime-js-init)

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "slime-js" '("slime-")))

;;;***

;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; slime-js-autoloads.el ends here
