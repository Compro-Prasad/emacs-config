;;; -*- no-byte-compile: t -*-
(define-package "eldoc-overlay" "20171219.140" "Display eldoc with contextual documentation overlay." '((emacs "24.3") (inline-docs "1.0.1") (quick-peek "1.0")) :commit "a391396f4cdf30a2f27a4c426b58b44ab3d0f0d0" :url "https://github.com/stardiviner/eldoc-overlay" :keywords '("documentation" "eldoc" "overlay"))
