;;; -*- no-byte-compile: t -*-
(define-package "zlc" "20151010.1857" "Provides zsh like completion system to Emacs" 'nil :commit "4dd2ba267ecdeac845a7cbb3147294ee7daa25f4" :keywords '("matching" "convenience"))
