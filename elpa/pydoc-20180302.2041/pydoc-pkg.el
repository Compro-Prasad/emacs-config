;;; -*- no-byte-compile: t -*-
(define-package "pydoc" "20180302.2041" "functional, syntax highlighted pydoc navigation" 'nil :commit "146a006493f5284fdf23b42ef90454f1be25d0c1" :url "https://github.com/statmobile/pydoc" :keywords '("pydoc" "python"))
