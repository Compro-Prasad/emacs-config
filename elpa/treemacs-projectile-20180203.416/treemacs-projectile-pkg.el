;;; -*- no-byte-compile: t -*-
(define-package "treemacs-projectile" "20180203.416" "Projectile integration for treemacs" '((projectile "0.14.0") (treemacs "0")) :commit "9ad516c8bd2a8db58ed9934420f6626921ab7b47" :url "https://github.com/Alexander-Miller/treemacs")
