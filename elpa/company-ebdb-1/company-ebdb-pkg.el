;;; -*- no-byte-compile: t -*-
(define-package "company-ebdb" "1" "company-mode completion backend for EBDB in message-mode" '((company "0.9.4") (ebdb "0.2")) :url "http://elpa.gnu.org/packages/company-ebdb.html" :keywords nil)
