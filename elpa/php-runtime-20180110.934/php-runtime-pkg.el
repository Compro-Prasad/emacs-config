;;; -*- no-byte-compile: t -*-
(define-package "php-runtime" "20180110.934" "Language binding bridge to PHP" '((emacs "25") (cl-lib "0.5")) :commit "fa4312863245511462b75cb31df2f8558288f4df" :url "https://github.com/emacs-php/php-runtime.el" :keywords '("processes" "php"))
