;;; -*- no-byte-compile: t -*-
(define-package "web-mode" "20180326.451" "major mode for editing web templates" '((emacs "23.1")) :commit "bb1f200d4d9ac19ea6997c8d52c5a6c329e6dd24" :url "http://web-mode.org" :keywords '("languages"))
