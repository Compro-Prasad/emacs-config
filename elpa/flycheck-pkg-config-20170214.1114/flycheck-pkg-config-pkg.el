;;; -*- no-byte-compile: t -*-
(define-package "flycheck-pkg-config" "20170214.1114" "configure flycheck using pkg-config" '((dash "2.8.0") (s "1.9.0") (flycheck "29")) :commit "fda3f828082bd957c838126577a6b7e4d747dd54" :keywords '("flycheck"))
