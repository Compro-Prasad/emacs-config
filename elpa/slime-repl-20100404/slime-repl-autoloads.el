;;; slime-repl-autoloads.el --- automatically extracted autoloads
;;
;;; Code:

(add-to-list 'load-path (directory-file-name
                         (or (file-name-directory #$) (car load-path))))


;;;### (autoloads nil "slime-repl" "slime-repl.el" (0 0 0 0))
;;; Generated autoloads from slime-repl.el

(autoload 'slime-repl-init "slime-repl" "\
\n\n(fn)" nil nil)

(add-hook 'slime-load-hook 'slime-repl-init)

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "slime-repl" '("interrupt-in-blocking-read" "repl-" "sldb-insert-frame-call-to-repl" "slime-" "with-canonicalized-slime-repl-buffer" "package-updating" "nil" "defslime-repl-shortcut" "?r")))

;;;***

;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; slime-repl-autoloads.el ends here
