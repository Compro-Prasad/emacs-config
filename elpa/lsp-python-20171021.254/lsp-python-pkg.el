;;; -*- no-byte-compile: t -*-
(define-package "lsp-python" "20171021.254" "Python support for lsp-mode" '((lsp-mode "3.0")) :commit "035fed681ef18a774dcb82e361bd6b5b8778623f" :url "https://github.com/emacs-lsp/lsp-python" :keywords '("python"))
