;;; -*- no-byte-compile: t -*-
(define-package "company-rtags" "20170924.2244" "RTags back-end for company" '((emacs "24.3") (company "0.8.1") (rtags "2.10")) :commit "6f411f840cc79e27ba40a6b92a65ecbd4a3e9397" :url "http://rtags.net")
