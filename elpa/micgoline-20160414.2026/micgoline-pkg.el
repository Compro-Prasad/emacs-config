;;; -*- no-byte-compile: t -*-
(define-package "micgoline" "20160414.2026" "powerline mode, color schemes from microsoft and google's logo." '((emacs "24.3") (powerline "2.3")) :commit "837504263bb1711203b0f7efecd6b7b5f272fae0" :url "https://github.com/yzprofile/micgoline" :keywords '("mode-line" "powerline" "theme"))
