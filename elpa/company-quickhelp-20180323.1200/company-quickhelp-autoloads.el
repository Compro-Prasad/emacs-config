;;; company-quickhelp-autoloads.el --- automatically extracted autoloads
;;
;;; Code:

(add-to-list 'load-path (directory-file-name
                         (or (file-name-directory #$) (car load-path))))


;;;### (autoloads nil "company-quickhelp" "company-quickhelp.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from company-quickhelp.el

(autoload 'company-quickhelp-local-mode "company-quickhelp" "\
Provides documentation popups for `company-mode' using `pos-tip'.\n\n(fn &optional ARG)" t nil)

(defvar company-quickhelp-mode nil "\
Non-nil if Company-Quickhelp mode is enabled.\nSee the `company-quickhelp-mode' command\nfor a description of this minor mode.\nSetting this variable directly does not take effect;\neither customize it (see the info node `Easy Customization')\nor call the function `company-quickhelp-mode'.")

(custom-autoload 'company-quickhelp-mode "company-quickhelp" nil)

(autoload 'company-quickhelp-mode "company-quickhelp" "\
Toggle Company-Quickhelp-Local mode in all buffers.\nWith prefix ARG, enable Company-Quickhelp mode if ARG is positive;\notherwise, disable it.  If called from Lisp, enable the mode if\nARG is omitted or nil.\n\nCompany-Quickhelp-Local mode is enabled in all buffers where\n`company-quickhelp-local-mode' would do it.\nSee `company-quickhelp-local-mode' for more information on Company-Quickhelp-Local mode.\n\n(fn &optional ARG)" t nil)

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "company-quickhelp" '("company-quickhelp-")))

;;;***

;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; company-quickhelp-autoloads.el ends here
