;;; -*- no-byte-compile: t -*-
(define-package "composer" "20180111.942" "Interface to PHP Composer" '((emacs "24") (s "1.9.0") (f "0.17") (request "0.2.0") (seq "1.9") (php-runtime "0.1.0")) :commit "e34ebe795d267e28965c85bd84cbb16b18165bd8" :url "https://github.com/zonuexe/composer.el" :keywords '("tools" "php" "dependency" "manager"))
