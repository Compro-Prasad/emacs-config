#+TITLE: All My Emacs Configurations
#+AUTHOR: Compro Prasad
#+STARTUP: logdone
#+STARTUP: indent

* Internals
These customizations don’t require any external dependency and can be
used with bare Emacs *25.3*.
** User details
Emacs should always know who you are and how to contact with the
outside world.  Not as necessary but it gives you the feeling of
owning Emacs. It does provides integration with mail utils and
contacts but I don’t use such things. But I do have plans.
#+BEGIN_SRC emacs-lisp -i
(setq user-mail-address "comproprasad@gmail.com"
      user-full-name "Abhishek(Compro) Prasad")
#+END_SRC

** Convert =yes= to =y= and =no= to =n=
Its really annoying when Emacs asks for *yes* / *no* every now and then. So,
better convert that to simple *y* / *n*.
#+BEGIN_SRC emacs-lisp -i
(defalias 'yes-or-no-p 'y-or-n-p)
#+END_SRC

** Separate file for auto Emacs customizations
Emacs litters =init-el= with =M-x= =customize= settings. So, I chose a
separate trash can for this purpose. If required I will pick
configurations from there and document it here.
#+BEGIN_SRC emacs-lisp -i
(setq custom-file (expand-file-name "custom.el" user-emacs-directory))
(load custom-file)
#+END_SRC

** Load git tokens
This is the file where I keep applications in Emacs authorized with
GitHub tokens. Since, not everyone may use this thing it is kept uder
a check.
#+BEGIN_SRC emacs-lisp -i
(if (file-readable-p "~/.git-tokens")
    (load-file "~/.git-tokens"))
#+END_SRC

** Get support for all characters
Emacs shows weird characters if =utf-8= isn't enabled.
#+BEGIN_SRC emacs-lisp -i
(set-language-environment 'utf-8)
(set-default-coding-systems 'utf-8)
(set-selection-coding-system 'utf-8)
(set-locale-environment "en.UTF-8")
(set-terminal-coding-system 'utf-8)
(set-keyboard-coding-system 'utf-8)
(prefer-coding-system 'utf-8)
(setq buffer-file-coding-system 'utf-8)
(setq x-select-request-type '(UTF8_STRING COMPOUND_TEXT TEXT_STRING))
#+END_SRC

** Font setting
Everybody likes this, so do I. Make sure you have it installed in your
system. [[https://fonts.google.com/download?family=Source%20Code%20Pro][Click here to download Source Code Pro font pack]].

Sometimes the font needs to be scaled down to as low as 10 or as high
as 14. So, whenever I need that I just do =M-x= =menu-set-font= which
brings me a font picker from where I can set the font for the current
session.
#+BEGIN_SRC emacs-lisp -i
(set-frame-font "Source Code Pro-12")
#+END_SRC

** Hide basic UI components
If you need some of the UI components just pass in a possitive value to the function.
#+BEGIN_SRC emacs-lisp -i
(menu-bar-mode 0)
(tool-bar-mode 0)
(menu-bar-no-scroll-bar)
(blink-cursor-mode -1)
#+END_SRC

** Mode line customization
Not necessary but it shows the column number in the current line.
#+BEGIN_SRC emacs-lisp -i
(column-number-mode 1)
#+END_SRC

** Overwrite marked regions
Modern editors delete a region of selected text on inserting a new
character while the region was selected.

This doesn’t happen by default in Emacs. Instead, Emacs keeps on
adding text in the current cursor position even when the region is
selected. So, there is this feature function called
=delete-selection-mode= which needs to be turned on in order to get
the default behaviour of the modern text editors.
#+BEGIN_SRC emacs-lisp -i
(delete-selection-mode 1)
#+END_SRC

** Auto complete the next pair
This is so good that web-mode also relies on its auto completion to
complete the ending tags.
#+BEGIN_SRC emacs-lisp -i
(electric-pair-mode 1)
#+END_SRC

** Unicode quotes if in normal text region/buffer
In a buffer where a character like =`= is turned into *‘’* which is
super awesome, though it will render badly in non utf 8 encodings.
#+BEGIN_SRC emacs-lisp -i
(electric-quote-mode t)
#+END_SRC

** Subword Mode
Jumps per word. If this is disabled Emacs would jump to the next word separated
by whitespace(s). But enabling this allows me to jump words separated by other
punctuation too. Its slower but it fits my needs.
#+BEGIN_SRC emacs-lisp -i
(setq global-subword-mode t)
#+END_SRC

** Xterm/Terminal mouse support
Enables mouse support if Emacs is initialized/opened from terminal.
#+BEGIN_SRC emacs-lisp -i
(if (not window-system)
    (xterm-mouse-mode 1)
  (xterm-mouse-mode 0))
#+END_SRC

** Maximize Emacs Frame
By default, Emacs opens up in a small window. So, its better to make it
full screen.
#+BEGIN_SRC emacs-lisp -i
(toggle-frame-maximized)
(toggle-frame-fullscreen)
#+END_SRC

** No tabs. Only spaces.
#+BEGIN_SRC emacs-lisp -i
(setq-default indent-tabs-mode nil)
#+END_SRC

** Enable features
Some features are by default disabled in Emacs to prevent a bad user
experience if you don't know about that specific feature.
#+BEGIN_SRC emacs-lisp -i
(setq disabled-command-function nil)
#+END_SRC
Source: [[https://github.com/wasamasa/dotemacs/blob/master/init.org#enable-every-deactivated-command][wasamasa - GitHub]]

** ERC (Emacs IRC)
#+BEGIN_SRC emacs-lisp -i
(use-package erc
  :ensure t :defer t
  :config
  (setq erc-hide-list '("PART" "QUIT" "JOIN")
        erc-server "irc.freenode.net"
        erc-nick "compro"))
#+END_SRC

** Bump up kill ring/clipboard max value
Emacs keeps track of word and line kills too. So, the actual things
that were copied are just pushed back. Since, the default maxima is 60
it is insufficient to get older pastes.
#+BEGIN_SRC emacs-lisp -i
(setq kill-ring-max 512)
#+END_SRC

** =imenu= - Jump to an identifier
#+BEGIN_SRC emacs-lisp -i
(use-package imenu
  :ensure t :defer t
  :bind ("<C-S-mouse-1>" . imenu))
#+END_SRC

** Auto refresh buffers when files change
#+BEGIN_SRC emacs-lisp -i
(global-auto-revert-mode t)
#+END_SRC

** Speedup display of characters
#+BEGIN_SRC emacs-lisp -i
(setq inhibit-compacting-font-caches t)
#+END_SRC

** Highlight Matching Parenthesis
#+BEGIN_SRC emacs-lisp -i
(show-paren-mode t)
#+END_SRC

** Right click on selection/region
This will give cut, copy, paste like actions on right click. The right
click might be on =mouse-2=, so you may need to change the
configuration.
#+BEGIN_SRC emacs-lisp -i
(define-key global-map [mouse-3] menu-bar-edit-menu)
#+END_SRC

** Set region colors
#+BEGIN_SRC emacs-lisp -i
(set-face-attribute 'region nil
                    :background "pale turquoise"
                    :distant-foreground "gtk_selection_fg_color")
#+END_SRC

** Decrease frequency of GC
#+BEGIN_SRC emacs-lisp -i
(setq gc-cons-threshold 10000000)
#+END_SRC

** Org mode configurations
#+BEGIN_SRC emacs-lisp -i
(setq org-src-fontify-natively t
      org-startup-indented t
      org-src-preserve-indentation t
      org-enforce-todo-dependencies t
      org-enforce-todo-checkbox-dependencies t

      org-ellipsis "⤵"

      org-export-backends '(ascii beamer html latex md)

      org-latex-listings 'minted
      org-latex-packages-alist '(("" "tabu") ("" "minted"))
      org-latex-pdf-process
      '("pdflatex -shell-escape -interaction nonstopmode -output-directory %o %f"
        "pdflatex -shell-escape -interaction nonstopmode -output-directory %o %f"))
#+END_SRC
Source: [[https://github.com/wasamasa/dotemacs/blob/master/init.org#org-mode][wasamasa - GitHub]]

*** Some handy keybindings
#+BEGIN_SRC emacs-lisp -i
(define-key global-map "\C-cl" 'org-store-link)
(define-key global-map "\C-cc" 'org-capture)
;; (define-key global-map "\C-cb" 'org-iswitchb)
(define-key global-map "\C-ca" 'org-agenda)
#+END_SRC

*** Org babel execution engines
#+BEGIN_SRC emacs-lisp -i
(require 'ob-dot)
(require 'ob-shell)
(require 'ob-C)
(require 'ob-shell)
(require 'ob-ruby)
(require 'ob-emacs-lisp)
(require 'ob-python)
(require 'ob-R)
(require 'ob-table)
(require 'ob-latex)
(require 'ob-calc)
(require 'ob-scheme)
(require 'ob-latex)
(require 'ob-matlab)
(require 'ob-java)
#+END_SRC

*** DONE Journals in Org Mode
CLOSED: [2018-04-07 Sat 10:11]
#+BEGIN_SRC emacs-lisp -i
(setq org-capture-templates
      '(
        ("j" "Journal Entry" entry
         (file+datetree "~/Dropbox/programs/notes/journal.org")
         "* %?\n\nSource: %a" :empty-lines 1)

        ;; ... other templates
        ))
#+END_SRC
Source: [[http://www.howardism.org/Technical/Emacs/journaling-org.html]]
** Manage trailing whitespaces
#+BEGIN_SRC emacs-lisp -i
;; Switch off the display of trailing whitespaces in all buffers
(setq-default show-trailing-whitespace nil)
(defun my/set-show-whitespace-mode ()
  "Show white space in current buffer"
  (setq show-trailing-whitespace t))
;; Show whitespaces only in buffers pointing to specific files
(add-hook 'find-file-hook 'my/set-show-whitespace-mode)
;; Remove the trailing whitespaces on save
(add-hook 'before-save-hook 'delete-trailing-whitespace)
#+END_SRC

** DONE Abbrev Mode
#+BEGIN_SRC emacs-lisp -i
(use-package abbrev
  :config
  (if (file-exists-p abbrev-file-name)
      (quietly-read-abbrev-file))
  (if (file-exists-p "./my-abbrev.el")
      (load "./my-abbrev.el")))
#+END_SRC

** Highlight current line
Highlight the line on which the cursor is sitting. This is good for
large table modes like the package lists.
#+BEGIN_SRC emacs-lisp -i
(add-hook 'package-menu-mode-hook 'hl-line-mode)
#+END_SRC

** Browser in Emacs
Now, Emacs users have the option to run a JavaScript enabled web
browser inside of Emacs if Emacs is compiled with ~--with-xwidgets~
configure option. But the defaults are not good to get into the
workflow, so [[https://www.reddit.com/user/tuhdo][tuhdo]] [[https://www.reddit.com/r/emacs/comments/4srze9/watching_youtube_inside_emacs_25/][posted]] about his configuration which is as follows:
#+BEGIN_SRC emacs-lisp -i
(when (fboundp 'xwidget-webkit-browse-url)
  (use-package xwidget
    :defer t
    :bind (:map xwidget-webkit-mode-map
                ([mouse-4] . xwidget-webkit-scroll-down)
                ([mouse-5] . xwidget-webkit-scroll-up)
                ("<up>" . xwidget-webkit-scroll-down)
                ("<down>" . xwidget-webkit-scroll-up)
                ("M-w" . xwidget-webkit-copy-selection-as-kill)
                ("C-c" . xwidget-webkit-copy-selection-as-kill))
    :hook (window-configuration-change-hook
           . (lambda ()
               (when (equal major-mode 'xwidget-webkit-mode)
                 (xwidget-webkit-adjust-size-dispatch))))
    :init
    ;; by default, xwidget reuses previous xwidget window,
    ;; thus overriding your current website, unless a prefix argument
    ;; is supplied
    ;;
    ;; This function always opens a new website in a new window
    (defun xwidget-browse-url-no-reuse (url &optional sessoin)
      (interactive
       (progn
         (require 'browse-url)
         (browse-url-interactive-arg "xwidget-webkit URL: ")))
      (xwidget-webkit-browse-url url t)))
  ;; make xwidget default browser
  (setq browse-url-browser-function
        (lambda (url session)
          (other-window 1)
          (xwidget-browse-url-no-reuse url))))
#+END_SRC
* Externals (plugins)
These are customizations that require internet access to download and
install emacs lisp extensions.
** Better Defaults
#+BEGIN_SRC emacs-lisp -i
(use-package better-defaults
  :defer t :ensure t)
#+END_SRC

** Some good themes
#+BEGIN_SRC emacs-lisp -i
(use-package monokai-theme   :ensure t :defer t)
(use-package solarized-theme :ensure t :defer t)
(use-package ahungry-theme   :ensure t :defer t)
(use-package zenburn-theme   :ensure t :defer t)
#+END_SRC

** Helm - Autocomplete emacs commands and other stuff
#+BEGIN_SRC emacs-lisp -i
(use-package helm
  :ensure t :defer t
  :bind (("M-x" . helm-M-x)
         ([f8] . helm-find-files)
         ([f9] . helm-buffers-list))
  :config
  (require 'helm-config)
  (helm-mode 1)
  (setq helm-M-x-fuzzy-match t))
#+END_SRC

** =yasnippet= - TextMate like snippets
#+BEGIN_SRC emacs-lisp -i
(use-package yasnippet
  :ensure t :defer t
  :init
  (use-package yasnippet-snippets
    :ensure t
    :defer t)
  (yas-global-mode 1))
#+END_SRC
+ Authors:
  - pluskid <pluskid@gmail.com>
  - João Távora <joaotavora@gmail.com>
  - Noam Postavsky <npostavs@gmail.com>
+ Maintainer: Noam Postavsky <npostavs@gmail.com>

** =flycheck= - Syntax check on the fly
Ever wondered to get to know the compilation errors before even
building the project? Yeah these things are already provided in IDEs
so why can’t Emacs do that.

=flymake= was the syntax checker before =flycheck= and it is still now
used by many people. But for me, =flycheck= does much work without
much of configuration or prerequisites like Makefiles.
#+BEGIN_SRC emacs-lisp -i
(use-package flycheck
  :ensure t :defer t
  :hook (prog-mode . flycheck-mode)
  :config
  (add-hook 'c++-mode-hook
            '(lambda ()
               (flycheck-select-checker 'c/c++-gcc)))
  )
#+END_SRC
Including different includes and libraries is a tedious job. So, for C
and C++ development a command line tool called =pkg-config= does the
job of providing a known library’s path and linking flags.

This Emacs package just uses =pkg-config= to provide these flags to
=flycheck= after selecting a library interactively. It also gives
completion of all the know libraries to =pkg-config=.
#+BEGIN_SRC emacs-lisp -i
(use-package flycheck-pkg-config
  :ensure t :defer t)
#+END_SRC
#+BEGIN_SRC emacs-lisp -i
(use-package flycheck-clang-analyzer
  :ensure t :defer t
  :config
  (flycheck-clang-analyzer-setup))
#+END_SRC

** GNU Global - =ggtags=
#+BEGIN_SRC emacs-lisp -i
(use-package ggtags
  :ensure t :defer t
  :hook ((c++-mode c-mode java-mode go-mode) . ggtags-mode))
#+END_SRC

** =magit= - Git Porcelain
#+BEGIN_SRC emacs-lisp -i
(use-package magit
  :ensure t :defer t
  :bind ("C-x g" . 'magit-status)
  ;; :hook (magit-post-refresh . diff-hl-magit-post-refresh)
  :config (setq ;; magit-diff-highlight-indentation t
                magit-diff-highlight-trailing t
                magit-diff-paint-whitespace t
                magit-diff-highlight-hunk-body t
                magit-diff-refine-hunk 'all))
#+END_SRC

** Hungry Deletion
Hungry delete is only present in =cc-mode=. What about other
modes. Thus this package from [[https://github.com/nflath][Nathaniel Flath]].
#+BEGIN_SRC emacs-lisp -i
(use-package hungry-delete
  :ensure t :defer t
  :init
  (global-hungry-delete-mode 1))
#+END_SRC

** Web mode
The one and only package which understands a wide range of liquid
templates from [[https://github.com/fxbois][fxbois]].
#+BEGIN_SRC emacs-lisp -i
(use-package web-mode
  :ensure t :defer t
  :mode "\\.\\(html\\|vue\\|htm\\)\\'")
#+END_SRC

** Expand region
The region auto completion without the need of a mouse from [[https://github.com/magnars][Magnar
Sveen]].
#+BEGIN_SRC emacs-lisp -i
(use-package expand-region
  :ensure t :defer t
  :bind (("C-=" . er/expand-region)
         ("C-+" . er/contract-region)))
#+END_SRC

** Project management using =projectile=
The best ever project management tool in Emacs from [[https://github.com/bbatsov/][Bozhidar Batsov]].
#+BEGIN_SRC emacs-lisp -i
(use-package projectile
  :ensure t :defer t
  :bind-keymap ("C-c p" . projectile-command-map)
  :init
  (use-package helm-projectile
    :ensure t
    :defer t
    :init
    (helm-projectile-on))
  :config
  (projectile-mode 1))
#+END_SRC

** Complete anything(=company=)
The code completion framework that is easy to work with.
#+BEGIN_SRC emacs-lisp -i
(use-package company
  :ensure t :defer t
#+END_SRC

** C/C++ completion
#+BEGIN_SRC emacs-lisp -i
(use-package company-irony
  :ensure t :defer t
  :init
  (global-company-mode 1))
#+END_SRC

** Better window switching
By default it is tedious to switch windows using =C-x o= repeatedly.
#+BEGIN_SRC emacs-lisp -i
(use-package ace-window
  :ensure t :defer t
  :bind ("M-o" . ace-window))
#+END_SRC

** Auto completion for keyboard shortcuts
We all know Emacs has many weird shortcuts. This package from [[https://github.com/justbur][Justin
Burkett]] solves this problem.
#+BEGIN_SRC emacs-lisp -i
(use-package which-key
  :ensure t :defer t
  :init
  (which-key-mode 1))
#+END_SRC

** TODO Tabs?
Here it is from [[https://github.com/Alexander-Miller][Alexander Miller]].
#+BEGIN_SRC emacs-lisp -i
;; Sorry, it is empty for now. I am yet deciding on what to use.
#+END_SRC

** Sidebar containing project
Good for browsing through a project from [[https://github.com/Alexander-Miller][Alexander Miller]].
#+BEGIN_SRC emacs-lisp -i
(use-package treemacs-projectile
  :ensure t :defer t
  :bind (([f5] . treemacs)
         ([f6] . treemacs-projectile)))
#+END_SRC

** Multiple cursors is here too
#+BEGIN_SRC emacs-lisp -i
(use-package multiple-cursors
  :ensure t :defer t
  :bind (("M-/" . mc--mark-symbol-at-point)
         ("C-S-c" . mc/edit-lines)
         ("M-S-<up>" . mc/mark-previous-like-this)
         ("M-<up>" . mc/skip-to-previous-like-this)
         ("M-S-<down>" . mc/mark-next-like-this)
         ("M-<down>" . mc/skip-to-next-like-this)
         ("C-c C-<" . mc/mark-all-like-this)
         ("M-S-<mouse-1>" . mc/add-cursor-on-click)
         ("M-S-<mouse-2>" . mc/add-cursor-on-click)
         ("M-S-<mouse-3>" . mc/add-cursor-on-click)))
#+END_SRC
/Author: Magnar Sveen <magnars@gmail.com>/

** Rename identifiers in a buffer
#+BEGIN_SRC emacs-lisp -i
(use-package iedit
  :ensure t :defer t)
#+END_SRC

** PDF tools in Emacs
#+BEGIN_SRC emacs-lisp -i
(use-package pdf-tools
  :defer t :ensure t
  :init
  (pdf-tools-install))
#+END_SRC

** Highlight diffs
This helps to highlight diffs in Emacs if the file is under version
control.
#+BEGIN_SRC emacs-lisp -i
(use-package diff-hl
  :defer t :ensure t
  :hook (prog-mode . diff-hl-mode)
  :init (progn
          (setq diff-hl-side 'right)
          (diff-hl-margin-mode t)))
          ;;(diff-hl-flydiff-mode t)))
#+END_SRC

** GitHubGist integration
Enables me to edit and upload new gists to GitHub. Might need a
setup. For me it worked without any setup.
#+BEGIN_SRC emacs-lisp -i
(use-package gist
  :defer t :ensure t)
#+END_SRC

** TODO Smart Mode Line
#+BEGIN_SRC emacs-lisp -i
(use-package smart-mode-line
  :ensure t :defer t
  :init
  (setq sml/theme 'dark
        sml/mode-width 'full
        sml/name-width '(0 . 30)
        sml/replacer-regexp-list '(("^~/org/" ":O:")
                                   ("^~/\\.emacs\\.d/" ":ED:"))
        rm-blacklist ".*")
  (sml/setup))
#+END_SRC
Source: [[https://github.com/wasamasa/dotemacs/blob/master/init.org#improve-the-mode-line][wasamasa - GitHub]]

* Custom Elisp
** Way to learn to use Emacs keybindings
It will do near about the same thing but its not what you wanted to happen.
So, its easier to use C-p, C-n, C-b and C-f in this case.
#+BEGIN_SRC emacs-lisp -i
(defun use-emacs-keybindings ()
  "Set up, down, left and right keys to scroll text instead of
  move the cursor."
  (interactive)
  (global-set-key [up] '(lambda () (interactive) (scroll-down 1)))
  (global-set-key [down] '(lambda () (interactive) (scroll-up 1)))
  (global-set-key [left] '(lambda () (interactive) (scroll-right 1)))
  (global-set-key [right] '(lambda () (interactive) (scroll-left 1))))
(defun use-normal-keybindings ()
  "Set up, down, left and right keys to move the cursor instead
  of scrolling the text."
  (interactive)
  (global-set-key [up] 'previous-line)
  (global-set-key [down] 'next-line)
  (global-set-key [left] 'backward-char)
  (global-set-key [right] 'forward-char))
(use-emacs-keybindings)
#+END_SRC

** Dedicated window toggle
Make a window dedicated to a buffer.
#+BEGIN_SRC emacs-lisp -i
(defun toggle-dedicated-window (&optional window)
  "Toggle the option of a buffer being dedicated to a window or not."
  (interactive)
  (if (null window)
      (setq window (selected-window)))
  (message
   (if (set-window-dedicated-p window
                               (not (window-dedicated-p window)))
       "Window '%s' is dedicated"
     "Window '%s' is normal") (current-buffer)))

(global-set-key (kbd "C-M-z") 'toggle-dedicated-window)
#+END_SRC

** =eshell= clear buffer
Clear eshell buffer using =C-l=.
#+BEGIN_SRC emacs-lisp -i
(defun clear-buffer (func)
  "Execute FUNC after clearing buffer."
  (let ((inhibit-read-only t))
    (erase-buffer)
    (funcall func)))
(defun eshell-clear-buffer ()
  "Clear terminal."
  (interactive)
  (clear-buffer 'eshell-send-input))
(add-hook 'eshell-mode-hook
          '(lambda()
             (local-set-key (kbd "C-l") 'eshell-clear-buffer)))
#+END_SRC

** Timestamp
#+BEGIN_SRC emacs-lisp -i
(defun insert-date ()
  "Insert current date yyyy-mm-dd."
  (interactive)
  (insert (format-time-string "[Timestamp: %c]")))
#+END_SRC
